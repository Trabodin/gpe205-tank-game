﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthEffect", menuName = "PickUpEffect/DamageBoost")]
public class DamageBoost : PickUpEffects {

    public int DamageBoosted;
    
    public override int doEffect(TankData data)
    {
       
        data.bulletDamage += DamageBoosted;
        return DamageBoosted;
    }

}
