﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthEffect", menuName = "PickUpEffect/SpeedBoost")]
public class SpeedBoost : PickUpEffects{

    public int SpeedBoosted;

    public override int doEffect(TankData data)
    {
       
        data.MoveSpeedForward += SpeedBoosted;
        return SpeedBoosted;
    }

}
