﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class PickUpEffects: ScriptableObject {

    public enum type {health,damage,speed};
    public type pickUptype;

    public virtual int doEffect(TankData data)
    {
        return 0;
    } 

}
