﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthEffect",menuName ="PickUpEffect/HealthEffect")]
public class HealthEffect : PickUpEffects {

    public int HealthRestored;
    
    public override int doEffect(TankData data)
    {
        
        data.Health += HealthRestored;
        return HealthRestored;
       
    }

 
}
