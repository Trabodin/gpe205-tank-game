﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

    public SpawnerPickUp spawner;
    public bool IsPermanatBuff;
    public float decayRatePerSecond;

    public PickUpEffects effect;
    public MeshRenderer meshRenderer;
    public BoxCollider boxCollider;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        TankData temp = other.GetComponent<TankData>();
        if(temp != null)
        {
            DoEffects(temp);
        }
    }

    //turns off the collied and renderer so it is esentially gone while it continues doing its effects
    void DoEffects(TankData data)
    {

        meshRenderer.enabled = false;
        boxCollider.enabled = false;
        int boostAmount = effect.doEffect(data);
        
        if (!IsPermanatBuff)
        {
            StartCoroutine(DecayPowerUp(boostAmount,decayRatePerSecond,data,effect.pickUptype));
        }
        else
        {

            if (spawner != null)
                spawner.PickUpHasBeenTaken = true;
            Destroy(gameObject);
        }
        
    }

    //checkes what types of power up was used so it can reverse the efects over time// couldnt do this in the other script sine there scriptable objects
    IEnumerator DecayPowerUp(int amountToDecay,float Decay, TankData data, PickUpEffects.type type)
    {
        if (spawner != null)
            spawner.PickUpHasBeenTaken = true;

        float decayLeft = amountToDecay;
        switch (type)
        {
            case PickUpEffects.type.health:
                while (decayLeft > 0)
                {
                    data.Health -= Decay * Time.deltaTime;
                    decayLeft -= Decay * Time.deltaTime;
                    yield return null;
                }
                break;
            case PickUpEffects.type.speed:
                while (decayLeft > 0)
                {
                    data.MoveSpeedForward -= Decay * Time.deltaTime;
                    decayLeft -= Decay * Time.deltaTime;
                    yield return null;
                }
                break;
            case PickUpEffects.type.damage:
                while (decayLeft > 0)
                {
                    data.bulletDamage -= Decay * Time.deltaTime;
                    decayLeft -= Decay * Time.deltaTime;
                    yield return null;
                }
                break;
        }
        Destroy(gameObject);
    }
}
