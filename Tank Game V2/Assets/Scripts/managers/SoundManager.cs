﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager soundManager;

    public delegate void OnSoundChanged();
    public static OnSoundChanged onSoundChanged = delegate { };


    [Header("Volume Settings")]
    [Range(0,1)]
    public float BGMVolume;
    [Range(0, 1)]
    public float SFXVolume;
    [Range(0, 1)]
    public float UIVolume;

    [Header("Not Position Dependent Audio Clips")]
    public List<Sound> clips;

	// Use this for initialization
	void Awake () {
		if(soundManager == null)
        {
            soundManager = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }

        foreach  (Sound clip in clips)
        {
            clip.source = gameObject.AddComponent<AudioSource>();
            clip.source.clip = clip.clip;
            clip.source.volume = GetVolume(clip.type);
            if (clip.Loops)
                clip.source.loop = true;
        }


        
	}
    private void Start()
    {
        onSoundChanged = UpdateVolume;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void UpdateVolume()
    {

        foreach (Sound clip in clips)
        { 
            clip.source.volume = GetVolume(clip.type);  
        }
    }
    //plays clip at point after it has given it the correct volume
    public void PlaySoundAtPoint(Sound clip,Vector3 position)
    {
        float Volume = 0;
        Volume = GetVolume(clip.type);

        AudioSource.PlayClipAtPoint(clip.clip, position, Volume);
    }

    //returns the volume amount based of the type of soound
    public float GetVolume(Sound.SoundType type)
    {
        float Volume = 0;
        switch (type)
        {
            case Sound.SoundType.BGM:
                Volume = BGMVolume;
                break;
            case Sound.SoundType.SFX:
                Volume = SFXVolume;
                break;
            case Sound.SoundType.UI:
                Volume = UIVolume;
                break;
        }

        return Volume;
    }

    /////get sound menu to open in game add game over screen and made the rect on the cameras actually get set

    //private void OnValidate()
    //{
    //    if(onSoundChanged != null)
    //        onSoundChanged();
    //}

    //searches for a soound that it holds and plays it
    public void PlaySound(string name)
    {

        foreach (Sound clip in clips)
        {
            if (clip.name.Equals(name))
            {
                clip.source.volume = GetVolume(clip.type);
                clip.source.Play();
                break;
            }
        }
    }

    //sets one music to active and truns off all the other musics
    public void PlayMusic(string name)
    {
        foreach (Sound clip in clips)
        {
            if (clip.name.Equals(name))
            {
                clip.source.volume = GetVolume(clip.type);
                clip.source.Play();
              
            }
            else
            {
                if(clip.type == Sound.SoundType.BGM)
                {
                    clip.source.Stop();
                }
            }

        }
    }
}
