﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public static MenuManager menuManager;

    public List<GameObject> Menus;

    
    private void Start()
    {
        if (menuManager == null)
        {
            menuManager = this; 
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void LoadMenu(GameObject menuToLoad)
    {
        foreach (GameObject menu in Menus)
        {
            menu.SetActive(false);
        }
        menuToLoad.SetActive(true);
    }

    public void PauseMenu()
    {
        if(Time.timeScale > 0)
        {

            Time.timeScale = 0;
            LoadMenu(Menus[0]);
        }
        else if(Time.timeScale <= 0)
        {

            Time.timeScale = 1;
            Menus[0].SetActive(false);
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    //0 for wasd, 1 for arrows
    public void changeControlScheme(int Playerindex)
    {
       
        if (MapGenerator.generator.playerControls[Playerindex] == PlayerConroller.ControlScheme.WASD)
        {
            MapGenerator.generator.playerControls[Playerindex] = PlayerConroller.ControlScheme.ArrowKeys;
        }
        else
        {
            MapGenerator.generator.playerControls[Playerindex] = PlayerConroller.ControlScheme.WASD;
        }
                
             
    }

    public void ChangeGameisRunning()
    {
        Gamemanager.instance.GameIsRunning = !Gamemanager.instance.GameIsRunning;
    }
}
