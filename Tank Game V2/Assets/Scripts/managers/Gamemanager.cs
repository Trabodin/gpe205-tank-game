﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour {
    public static Gamemanager instance;
    public List<PlayerConroller> players;
    public List<AIPawn> Enemies;
    public MapGenerator generator;
    public GameObject MainCam;

    public int NumberOfPlayers=1;

    public bool GameIsRunning = false;

    // Use this for initialization
    void Awake() {
		if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}

    private void Start()
    {
        SoundManager.soundManager.PlayMusic("MenuMusic");
    }

    //clears the map and loads the last menu wich is the game over menu also re enables the camera sice the others attached tot the tanks get deleted
    public void GameOver()
    {
        MapGenerator.generator.clearMap();
        MenuManager.menuManager.LoadMenu( MenuManager.menuManager.Menus[MenuManager.menuManager.Menus.Count - 1]);
        MainCam.SetActive( true);

    }

    public void ChangeNumOfPlayers(int num)
    {
        NumberOfPlayers = num;
    }
}
