﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class to hold data about a particular soud clip
[System.Serializable]
public class Sound  {
    public enum SoundType
    {
        BGM, SFX, UI
    }

    public string name;
    public SoundType type;
    public AudioClip clip;
    [HideInInspector]
    public AudioSource source;
    public bool Loops;
}
