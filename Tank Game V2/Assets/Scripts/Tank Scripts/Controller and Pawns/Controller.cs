﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour {

    //every controller will get a pawn and tankengine

    public Pawn pawn;
    public TankEngine engine;

	// Use this for initialization
	public virtual void Start () {
        pawn = GetComponent<Pawn>();
        engine = GetComponent<TankEngine>();
	}

    public virtual void Onhit(float Damage, TankEngine shooter)
    {
        
    }
}
