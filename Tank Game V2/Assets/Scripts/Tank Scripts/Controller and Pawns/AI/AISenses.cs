﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISenses : MonoBehaviour {
    public float hearDistance;
    public float FOVangle;
    public float seeingDistance;

    //gets a ray cast to the target for the seeing distaqnce and if it hits the player it returns true
    public bool CanSee(GameObject target)
    {
        Vector3 vectorToPlayer = target.transform.position - transform.position;
        if (Vector3.Angle(transform.forward, vectorToPlayer) < FOVangle)
        {
            Ray ray = new Ray(transform.position, vectorToPlayer);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, seeingDistance);

           // Debug.Log(hit.collider);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    //if the noise of the target is greater than the distance to the target it returns true
    public bool CanHear(GameObject target)
    {
        NoiseMaker noise = target.GetComponent<NoiseMaker>();
        if(noise == null)
        {
            return false;
        }

        if(noise.Volume >= Vector3.Distance(transform.position, target.transform.position))
        {
            return true;
        }
        return false;
    }
}
