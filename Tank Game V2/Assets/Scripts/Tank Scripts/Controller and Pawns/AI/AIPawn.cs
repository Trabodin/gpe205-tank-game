﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPawn : Pawn {

    public override void Start()
    {
        base.Start();
        Gamemanager.instance.Enemies.Add(this);
        
    }



    //adds score for the player plays the death sound and removes from the list of enemies
    public override void Die()
    {
        SoundManager.soundManager.PlaySoundAtPoint(data.DieSound, transform.position);
        //AudioSource.PlayClipAtPoint(data.DieSound.clip,transform.position);
        Gamemanager.instance.Enemies.Remove(this);
        Destroy(gameObject);
    }
}
