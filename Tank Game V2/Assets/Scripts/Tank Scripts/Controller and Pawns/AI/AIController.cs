﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller
{

    //other scripts for the senseing and raycasting logic
    AISenses sense;
    VehicleSensors sensors;

    public float avoidanceDistance;
    public AITankPersonality AIpersonality;
    public List<Transform> patrolPoints = new List<Transform>();
    public float distBuffer;
    public int currentPatrolPoint;
    public float maxDistanceFromHome;

    Vector3 HomePoint;
    float distanceFromHome;
    bool isAvoiding;
    PlayerConroller Target;


    //enum for the FSM 
    public enum State { idle, patrol, hear, chase, attack, flee };
    public State currentState;


    public override void Start()
    {
        base.Start();
        currentPatrolPoint = 0;
        sense = GetComponent<AISenses>();
        sensors = GetComponent<VehicleSensors>();
        HomePoint = transform.position;
        isAvoiding = false;

        if (AIpersonality.usesPatroling)
        {
            currentState = State.patrol;
        }
        else
        {
            currentState = State.idle;
        }

    }

    void Update()
    {
        //only some of the states reqire obstical avoidance , i dont want it to avoid if it isnt moving or just rotaitng around
        switch (currentState)
        {
            case State.chase:
                Chase();
                ObsticalAvoidance();
                break;
            case State.attack:
                Attack();
                break;
            case State.flee:
                ObsticalAvoidance();
                flee();
                break;
            case State.hear:
                Hear();
                break;
            case State.patrol:
                ObsticalAvoidance();
                Patrol();
                break;
            case State.idle:
                Idle();
                break;
        }


    }


    // the tank will run back to its initial location
    void flee()
    {
        Vector3 vectorToHome = HomePoint - pawn.transform.position;
        engine.RotateToLook(Quaternion.LookRotation(vectorToHome));
        engine.MoveForward(1);

        // once the tank has reached its home spot it will return to idle or patrolling
        if (vectorToHome.magnitude < distBuffer)
        {
            if (AIpersonality.usesPatroling)
            {
                currentState = State.patrol;
            }
            else
            {
                currentState = State.idle;
            }
        }
    }

    //currently if the enemy hears the player it will just look at the player
    void Hear()
    {
        if(Target != null)
        {
            Vector3 vectorToPlayer = Target.transform.position - pawn.transform.position;
            engine.RotateToLook(Quaternion.LookRotation(vectorToPlayer));
        }
        

        foreach (PlayerConroller player in Gamemanager.instance.players)
        {
            //if it see the player or if it cant  then it changes state based on the personality
            if (sense.CanSee(player.gameObject) && AIpersonality.chasePlayerWhenSeen)
            {

                currentState = State.chase;
                Target = player;
                break;
            }
            else if (sense.CanSee(player.gameObject) && !AIpersonality.chasePlayerWhenSeen)
            {
                currentState = State.attack;
                Target = player;
                break;
            }
            if (!sense.CanHear(player.gameObject))
            {
                if (AIpersonality.usesPatroling)
                {
                    currentState = State.patrol;
                }
                else
                {
                    currentState = State.idle;
                    Target = null;
                }
                
            }

        }


    }
    //idle disent do anything i might make it do something later
    void Idle()
    {
        //changes state if it is in the wrong one
        if (AIpersonality.usesPatroling)
        {
            currentState = State.patrol;
            return;
        }
        foreach (PlayerConroller player in Gamemanager.instance.players)
        {
            if (sense.CanSee(player.gameObject))
            {
                currentState = State.chase;
                Target = player;
                break;
            }
            else if (sense.CanHear(player.gameObject))
            {
                currentState = State.hear;
                Target = player;
                break;
            }
        }
    }

    //the obstical avoidance uses the Vehichle sensors script for the raycasts
    void ObsticalAvoidance()
    {
        isAvoiding = false;
        //it avoids in the direction the normal of the surface the middle raycast hits
        if (sensors.hitFront.collider != null)
        {
            //this just gets the sign for what direction it need to turn in to avoid it
            isAvoiding = true;
            float avoidDirection = Mathf.Sign(Vector3.Cross(sensors.hitFront.normal, engine.tf.forward).y);


            if (sensors.hitFront.collider.gameObject.tag != "Player")
            {
                engine.Rotate(-avoidDirection * engine.data.RotateSpeedAvoid);
            }


        }
        //with the right and left they just rotate away from what side they are on
        else if (sensors.hitLeft.collider != null)
        {
            if (sensors.hitLeft.collider.gameObject.tag != "Player")
            {
                engine.Rotate(engine.data.RotateSpeedAvoid);
            }
        }
        else if (sensors.hitRight.collider != null)
        {

            if (sensors.hitRight.collider.gameObject.tag != "Player")
            {
                engine.Rotate(-engine.data.RotateSpeedAvoid);
            }
        }

        return;
    }

    //the tank will move forward and rotate at the player and shoot at hit if he is in front
    void Chase()
    {
        // if there is something it need to avoid it does that over looking at the player
        Vector3 vectorToPlayer = Target.transform.position - pawn.transform.position;
        if (!isAvoiding)
        {
            engine.RotateToLook(Quaternion.LookRotation(vectorToPlayer));
        }

        if (Vector3.Angle(pawn.transform.forward, vectorToPlayer) < 10)
        {
            engine.Shoot();
        }

        if(vectorToPlayer.magnitude > 50)
            engine.MoveForward(1);

        foreach (PlayerConroller player in Gamemanager.instance.players)
        {
            // if it cant see but it can hear the player then it goes to hearing if it cant hear or see the player then it changes back ased on personality
            if (!sense.CanSee(player.gameObject.gameObject))
            {
                if (sense.CanHear(player.gameObject.gameObject))
                {
                    currentState = State.hear;
                    Target = player;
                    break;
                }
                else if (!sense.CanHear(player.gameObject.gameObject))
                {
                    if (AIpersonality.usesPatroling)
                    {
                        currentState = State.patrol;
                        Target = null;
                    }
                    else
                    {
                        currentState = State.idle;
                        Target = null;
                    }

                }
            }

        }

        //the tank will flee if it has lower health than the health percentage in the personality or it is too far from home
        distanceFromHome = Vector3.Distance(engine.tf.position, HomePoint);
        if (AIpersonality.fleeHealthPercentage >= ((pawn.data.Health / pawn.data.maxHealth) * 100) || distanceFromHome > maxDistanceFromHome)
        {
            currentState = State.flee;
        }
    }

    // this is for tanks that dont do chasing, it jsut roates and shoots at the player
    void Attack()
    {
        if (Target != null)
        {
            Vector3 vectorToPlayer = Target.transform.position - pawn.transform.position;
            engine.RotateToLook(Quaternion.LookRotation(vectorToPlayer));

            if (Vector3.Angle(pawn.transform.forward, vectorToPlayer) < 10)
            {
                engine.Shoot();
            }

            foreach (PlayerConroller player in Gamemanager.instance.players)
            {
                
                if (!sense.CanSee(player.gameObject.gameObject))
                {
                    if (sense.CanHear(player.gameObject.gameObject))
                    {
                        currentState = State.hear;
                        Target = player;
                        break;
                    }
                    else if (!sense.CanHear(player.gameObject.gameObject))
                    {
                        if (AIpersonality.usesPatroling)
                        {
                            currentState = State.patrol;
                            
                        }
                        else
                        {
                            currentState = State.idle;
                            
                        }

                    }

                }
            }
        }

        if (AIpersonality.fleeHealthPercentage >= (pawn.data.Health / pawn.data.maxHealth * 100) || distanceFromHome > maxDistanceFromHome)
        {
            currentState = State.flee;
        }

    }

    // will patroll through all the points in a loop order and changes state if it sees or hears the player
    void Patrol()
    {
        if (patrolPoints != null)
        {
            if (patrolPoints.Count > 0)
            {

                Vector3 vectorToWaypoint = patrolPoints[currentPatrolPoint].position - pawn.transform.position;
                if (vectorToWaypoint.magnitude <= distBuffer)
                {
                    if (currentPatrolPoint == patrolPoints.Count - 1)
                    {
                        currentPatrolPoint = 0;
                    }
                    else
                    {
                        currentPatrolPoint++;
                    }
                }

                if (!isAvoiding)
                {
                    engine.RotateToLook(Quaternion.LookRotation(vectorToWaypoint));
                }

                engine.MoveForward(1);

            }

            foreach (PlayerConroller player in Gamemanager.instance.players)
            {
                if (sense.CanSee(player.gameObject))
                {
                   
                    currentState = State.chase;
                    Target = player;

                    break;
                }
                else if (sense.CanHear(player.gameObject))
                {
                    currentState = State.hear;
                    Target = player;
                    break;
                }
            }
        }

    }
    //does damage and inciments the person who killed its score
    public override void Onhit(float Damage, TankEngine shooter)
    {
        pawn.data.Health -= Damage;
        if (pawn.data.Health <= 0)
        {
            shooter.data.Score = pawn.data.ScoreValue + pawn.data.Score + shooter.data.Score;
            pawn.Die();

        }
    }
}
