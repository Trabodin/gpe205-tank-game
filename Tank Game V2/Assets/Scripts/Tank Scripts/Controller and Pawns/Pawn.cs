﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class to handle things like getting destroyed, pretty much anything that the enginge class dosent cover
/// </summary>
public abstract class Pawn : MonoBehaviour {

    public TankData data;

    public int LivesRemaining;


    public virtual void Start()
    {
        data = GetComponent<TankData>();
        data.Health = data.maxHealth;
    }

    public virtual void Onhit(float Damage,TankEngine Shooter) { }


    public virtual void Die() { }
   

}


