﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookScript : MonoBehaviour {

    public GameObject Turret;
    public GameObject Cannon;

    public float MouseSenseitivity;

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void Rotation()
    {
        float xAxis = Input.GetAxis("Mouse X") * MouseSenseitivity * Time.deltaTime;
        float yAxis = Input.GetAxis("Mouse Y") * MouseSenseitivity * Time.deltaTime;

        Cannon.transform.Rotate(-Cannon.transform.right * yAxis);
    }
}
