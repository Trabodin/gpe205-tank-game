﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPawn : Pawn {
    public GameObject thirdPersonCam;
    public Vector3 thirdPersonOffset;
    public GameObject FPSCam;
    public Vector3 firstPersonOffset;
    public GameObject FPSOverlay;
    public GameObject UIcanvas;

    TankEngine engine;

    GameObject FObj;
    GameObject TObj;
    CameraFollower TCam;
    CameraFollower FCam;
    UICanvas Ui;

    // Use this for initialization
    public override void Start () {
        //engine = GetComponent<TankEngine>();

        //TObj = Instantiate(thirdPersonCam, transform.position, Quaternion.identity);
        //TCam = TObj.GetComponent<CameraFollower>();
        //TCam.tankToFollow = engine.tf;
        //TCam.offset = thirdPersonOffset;
        //TCam.lookPosition = engine.tf;
        //TObj.SetActive(true);

        //FObj = Instantiate(FPSCam, transform.position, Quaternion.identity);
        //FCam = FObj.GetComponent<CameraFollower>();
        //FCam.tankToFollow = engine.tf;
        //FCam.offset = firstPersonOffset;
        //FCam.lookPosition = engine.FirePosition;
        //FObj.SetActive(false);
        base.Start();
	}

    public override void Die()
    {
        //some animation or noise
        PlayerConroller temp = gameObject.GetComponent<PlayerConroller>();
        temp.enabled = false;
        Gamemanager.instance.players.Remove(temp);
        Gamemanager.instance.NumberOfPlayers--;
        Destroy(FObj);
        Destroy(TObj);
        Destroy(Ui.gameObject);
        if(Gamemanager.instance.players.Count > 0)
            Gamemanager.instance.players[0].Ppawn.ResizeCameras(0);
        
        //TODO: show some gameover screen
    }

   

    public void SwapCamera()
    {
        if (FObj.activeInHierarchy)
        {
            Debug.Log("Active");
            FObj.SetActive(false);
            FPSOverlay.SetActive(false);
            TObj.SetActive(true);
        }
        else //(rdPersonCam.activeInHierarchy)
        {
            Debug.Log("NotActive");
            FObj.SetActive(true);
            FPSOverlay.SetActive(true);
            TObj.SetActive(false);
        }
    }

    public void InstatiateCameras()
    {
         engine = GetComponent<TankEngine>();

        TObj = Instantiate(thirdPersonCam, transform.position, Quaternion.identity);
        TCam = TObj.GetComponent<CameraFollower>();
        TCam.tankToFollow = engine.tf;
        TCam.offset = thirdPersonOffset;
        TCam.lookPosition = engine.tf;
        TObj.SetActive(true);

        FObj = Instantiate(FPSCam, transform.position, Quaternion.identity);
        FCam = FObj.GetComponent<CameraFollower>();
        FCam.tankToFollow = engine.tf;
        FCam.offset = firstPersonOffset;
        FCam.lookPosition = engine.FirePosition;
        FObj.SetActive(false);

        Ui = Instantiate(UIcanvas, transform.position, Quaternion.identity).GetComponent<UICanvas>();
        Ui.canvas.renderMode = RenderMode.ScreenSpaceCamera;
        
        Ui.canvas.worldCamera = TObj.GetComponent<Camera>();
        PlayerConroller player = GetComponent<PlayerConroller>();

        foreach (UIItem item in Ui.UI)
        {
            item.player = player;
            player.UI.Add(item);
        }
    }
    public void ResizeCameras(int numOfCams)
    {

        Camera cam;
        if (Gamemanager.instance.NumberOfPlayers == 1)
        {
            cam = TCam.GetComponent<Camera>();
            cam.rect = new Rect(0, 0, 1, 1);
            cam = FCam.GetComponent<Camera>();
            cam.rect = new Rect(0, 0, 1, 1);
        }
        else if (Gamemanager.instance.NumberOfPlayers == 2)
        {
            //sets the first to 0 then the second to .5
            cam = TCam.GetComponent<Camera>();
            cam.rect = new Rect(0, (.5f * numOfCams), 1, .5f);
            cam = FCam.GetComponent<Camera>();
            cam.rect = new Rect(0, (.5f * numOfCams), 1, .5f);
        }
    }
}
