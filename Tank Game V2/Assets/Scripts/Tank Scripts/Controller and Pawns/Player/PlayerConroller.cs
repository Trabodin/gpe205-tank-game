﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConroller : Controller {

    //floats to hold the directional input data
    float VerticalAxis = 0;
    float HorizontalAxis = 0;
    public PlayerPawn Ppawn;
    public NoiseMaker noiseMaker;

    public List<UIItem> UI;
    public enum ControlScheme { WASD, ArrowKeys}
    public ControlScheme controlScheme = ControlScheme.WASD;

    public override void Start()
    {
        //sets the player variable in the gameanger to this
        Gamemanager.instance.players.Add( this);
        Ppawn = GetComponent<PlayerPawn>();
        noiseMaker = GetComponent<NoiseMaker>();

        UpdateUI();
        base.Start();
    }
	
	// Update is called once per frame
	void Update () {
        //calls the diffrent pawn move functions depending on the getaxis being positive or negative if its 0 that means nothing is beign pressed
        switch (controlScheme)
        {
            case ControlScheme.WASD:
                WadMoveMent();
                break;
            case ControlScheme.ArrowKeys:
                ArrowKeyMovement();
                break;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            engine.Shoot();
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.shootNoise);
            }
            UpdateUI();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Ppawn.SwapCamera();
            
        }

   

    }

    void WadMoveMent()
    {
        VerticalAxis = Input.GetAxisRaw("Vertical");
        if (Input.GetKey(KeyCode.W))
        {
            engine.MoveForward(1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            engine.MoveBackward(-1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }

        HorizontalAxis = Input.GetAxisRaw("Horizontal");
        if (Input.GetKey(KeyCode.A))
        {
            engine.Rotate(-1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            engine.Rotate(1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
          
            MenuManager.menuManager.PauseMenu();

        }
    }

    public void ArrowKeyMovement()
    {
        VerticalAxis = Input.GetAxisRaw("Vertical");
        if (Input.GetKey(KeyCode.UpArrow))
        {
            engine.MoveForward(1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            engine.MoveBackward(-1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }

        HorizontalAxis = Input.GetAxisRaw("Horizontal");
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            engine.Rotate(-1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            engine.Rotate(1);
            //adds noise that the AI can hear
            if (noiseMaker != null)
            {
                noiseMaker.Volume = Mathf.Max(noiseMaker.Volume, pawn.data.moveNoise);
            }
        }
    }
    public override void Onhit(float Damage, TankEngine shooter)
    {
        //dies if you run out of health
        pawn.data.Health -= Damage;
        if (pawn.data.Health <= 0)
        {
            shooter.data.Score += pawn.data.ScoreValue;
            if (pawn.LivesRemaining > 0)
            {
                pawn.LivesRemaining--;
                Respawn();
            }
            else
            {
                if(Gamemanager.instance.players.Count > 1)
                {
                    pawn.Die();
                    
                   
                }
                else
                {
                    pawn.Die();
                   // this.enabled = false;
                  //  Gamemanager.instance.players.Remove(this);
                   // Gamemanager.instance.NumberOfPlayers--;
                    Gamemanager.instance.GameOver();
                }
                

            }

        }
        UpdateUI();
    }

    public void Respawn()
    {
        engine.data.Health = engine.data.maxHealth;
        MapGenerator.generator.SpawnPlayer(this);
    }

    private void UpdateUI()
    {
        foreach (UIItem item in UI)
        {
            item.UpdateUI();
        }
    }

    
}
