﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour {

    public float Volume;
    public float DecayPerSecond;

	// Update is called once per frame
	void Update () {
        //decays the noises over time
        if (Volume > 0)
        {
            Volume -= DecayPerSecond * Time.deltaTime;
        }
    }
}
