﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// hold the movement functions for the tanks 
/// </summary>
public class TankEngine : MonoBehaviour {
    public Transform tf;
    public GameObject Bullet;
    public Transform FirePosition;
    public TankData data;


    public AudioSource Auds;

    public float nextTimetoShoot;
    //TODO: Scriptable objects for diffrent personalities

    void Start()
    {

        nextTimetoShoot = 0;
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
        Auds = GetComponent<AudioSource>();
        //plays the running engine sound constantly
        Auds.clip = data.EngineSound.clip;
        ChangeVolume();
        Auds.Play();

        SoundManager.onSoundChanged += ChangeVolume;
    }

    public void MoveForward(float dir)
    {
        if (tf != null)
            tf.transform.Translate(Vector3.forward * data.MoveSpeedForward * dir * Time.deltaTime);
    }
    public void MoveBackward(float dir)
    {
        tf.transform.Translate(Vector3.forward * data.MoveSpeedBack * dir * Time.deltaTime);
    }


    public void Rotate(float dir)
    {
        tf.Rotate(Vector3.up, data.RotateSpeed * dir * Time.deltaTime);
    }
    public void RotateToLook(Quaternion target)
    {
        if(tf != null)
             tf.rotation = Quaternion.RotateTowards(tf.rotation, target, data.RotateSpeed* Time.deltaTime);
    }

    //instantiated a bullet and sets all its variables to the ones from this tanks tankdata component
    public void Shoot()
    {

        if (Time.time > nextTimetoShoot)
        {
           // SoundManager.soundManager.PlaySoundAtPoint(data.ShootSound, FirePosition.position, Sound.SoundType.SFX);
            Auds.PlayOneShot(data.ShootSound.clip,SoundManager.soundManager.GetVolume(data.ShootSound.type));
            Bullet SpawnedBullet = Instantiate(Bullet, FirePosition.position, tf.rotation).GetComponent<Bullet>();
            SpawnedBullet.damage = data.bulletDamage;
            SpawnedBullet.speed = data.bulletSpeed;
            SpawnedBullet.dieTime = data.bulletDieTime;
            SpawnedBullet.Shooter = this;
            nextTimetoShoot = Time.time + data.fireRateDelay;
        }

    }

    private void ChangeVolume()
    {
        Auds.volume = SoundManager.soundManager.SFXVolume - .2f;
    }

    private void OnDestroy()
    {
        SoundManager.onSoundChanged -= ChangeVolume;
    }
}
