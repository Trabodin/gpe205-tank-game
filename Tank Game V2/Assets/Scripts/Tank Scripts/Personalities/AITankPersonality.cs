﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// AI personality that dictates which funcions/states the AI will use
/// </summary>
[CreateAssetMenu(fileName = "AITankPersonality",menuName = "AITankPersonality")]
public class AITankPersonality : ScriptableObject {
    [Range(0, 100)]
    public float fleeHealthPercentage;

    public bool chasePlayerWhenSeen;
    public bool usesPatroling;
}
