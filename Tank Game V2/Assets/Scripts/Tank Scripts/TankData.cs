﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// just holds the data for all the diffrent tanks 
/// </summary>
public class TankData : MonoBehaviour {

    public float RotateSpeed;
    public float RotateSpeedAvoid;
    public float MoveSpeedForward;
    public float MoveSpeedBack;


    public float maxHealth;
    public float Health;
    public int ScoreValue;
    public float moveNoise;
    public float shootNoise;

    public int Score;

    public float bulletDamage;
    public float bulletSpeed;
    public float bulletDieTime;
    public float fireRateDelay;

    [Header("Audio")]
    public Sound ShootSound;
    public Sound EngineSound;
    public Sound DieSound;

}
