﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSensors : MonoBehaviour
{
    [Header("Debug Tools")]
    public Color GizmosColor;
    public float SphereRadius;

    [Header("Sensors")]
    public float sensorRange;
    public Vector3 SensorOffset;
    public float SensorSeperation;
    [Tooltip("Set the angle for the side sensors in degrees. 0 is straight forward")]
    [Range(0,180)]
    public float EdgeSensorAngle;

    private Transform tf;
    private Vector3 sensorPosition;
    private Vector3 sensorPositionRight;
    private Vector3 sensorPositionLeft;

    //Hits so you can accsess them in other scripts
    [HideInInspector]
    public RaycastHit hitFront;
    [HideInInspector]
    public RaycastHit hitLeft;
    [HideInInspector]
    public RaycastHit hitRight;

    // Use this for initialization
    void Start ()
	{
	    tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		Sensors();
	}

    public void Sensors()
    {
        //sets the origin of the main sensor 
        sensorPosition = tf.position;
        sensorPosition += (SensorOffset.z * tf.forward);
        sensorPosition += (SensorOffset.y * tf.up);
        sensorPosition += (SensorOffset.x * tf.right);

        //sets the side sensors based on the main sensor and a offset
        sensorPositionRight = sensorPosition + (SensorSeperation * tf.right);
        sensorPositionLeft = sensorPosition + (SensorSeperation * -tf.right);

        //Raycasts the angle is determined by the anglevalue set in the inspector from tf.forward
        Ray rayForward = new Ray(sensorPosition,tf.forward);
        Ray angledRayRigth= new Ray(sensorPositionRight, Quaternion.AngleAxis(EdgeSensorAngle, transform.up) * tf.forward);
        Ray angledRayLeft = new Ray(sensorPositionLeft, Quaternion.AngleAxis(-EdgeSensorAngle, transform.up) * tf.forward);

        Physics.Raycast(rayForward, out hitFront, sensorRange);
        Physics.Raycast(angledRayLeft, out hitLeft, sensorRange);
        Physics.Raycast(angledRayRigth, out hitRight, sensorRange);

    }

    private void OnDrawGizmosSelected()
    {
        //since it runs in editor i had to put this math here too
        sensorPosition = transform.position;
        sensorPosition += (SensorOffset.z * transform.forward);
        sensorPosition += (SensorOffset.y * transform.up);
        sensorPosition += (SensorOffset.x * transform.right);

        sensorPositionRight = sensorPosition + (SensorSeperation * transform.right);
        sensorPositionLeft = sensorPosition + (SensorSeperation * -transform.right);

        Gizmos.color = GizmosColor;
        Gizmos.DrawWireSphere(sensorPosition, SphereRadius);
        Gizmos.DrawWireSphere(sensorPositionLeft, SphereRadius);
        Gizmos.DrawWireSphere(sensorPositionRight, SphereRadius);

        Gizmos.DrawLine(sensorPosition,sensorPosition + transform.forward *sensorRange);

        Gizmos.DrawLine(sensorPositionLeft, sensorPositionLeft + (Quaternion.AngleAxis(-EdgeSensorAngle,transform.up) * transform.forward) * sensorRange);
        Gizmos.DrawLine(sensorPositionRight, sensorPositionRight + (Quaternion.AngleAxis(EdgeSensorAngle, transform.up) * transform.forward) * sensorRange);

        //reminder on how to do it with vector math instead of angles
        //  Gizmos.DrawLine(sensorPositionRight, sensorPositionRight + (transform.forward + (transform.right * EdgeSensorAngle)) * sensorRange);
        //  Gizmos.DrawLine(sensorPositionLeft, sensorPositionLeft + (transform.forward-(transform.right * EdgeSensorAngle)) * sensorRange);
    }
}
