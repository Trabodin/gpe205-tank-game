﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

    public Transform tankToFollow;
    public Transform lookPosition;
    public Vector3 offset;

    Transform tf;
	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        tf.position = tankToFollow.position + (tankToFollow.rotation * Quaternion.Euler(0,-90,0))* offset;
        
        tf.LookAt(lookPosition);
	}
}
