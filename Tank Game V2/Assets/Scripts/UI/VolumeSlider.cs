﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour {

    public Slider slider;
    public Sound.SoundType type;
	// Use this for initialization
	void Start () {
        slider.value = SoundManager.soundManager.GetVolume(type);
	}
	
    public void ChangeValue()
    {
        switch (type)
        {
            case Sound.SoundType.BGM:
               SoundManager.soundManager.BGMVolume  = slider.value;
                break;
            case Sound.SoundType.SFX:
                SoundManager.soundManager.SFXVolume = slider.value;
                break;
            case Sound.SoundType.UI:
                SoundManager.soundManager.UIVolume = slider.value;
                break;
        }

        if(SoundManager.onSoundChanged != null)
            SoundManager.onSoundChanged();
    }

}
