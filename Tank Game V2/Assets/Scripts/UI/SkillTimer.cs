﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SkillTimer : UIItem {

    public Image cooldownImage;

    public override void UpdateUI()
    {
        float timer = player.engine.nextTimetoShoot - Time.time;
        StartCoroutine(CooldownTimer(timer));
    }
    // Update is called once per frame
   
    IEnumerator CooldownTimer(float timer)
    {
        while (timer >0)
        {
            cooldownImage.fillAmount = timer / 1;
            timer -= Time.deltaTime;
            yield return null;
        }
       
    }
}
