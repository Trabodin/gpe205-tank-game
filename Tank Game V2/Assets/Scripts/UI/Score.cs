﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : UIItem {
    public Text text;

    public override void UpdateUI()
    {
        text.text = "Score : " + player.engine.data.Score.ToString();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       
	}
}
