﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesRemainingUI :UIItem {

   public List<Image> images;

    public override void UpdateUI()
    {
        int i = 0;
        for (; i < player.Ppawn.LivesRemaining; i++)
        {
            images[i].enabled = true;
        }
        for (; i < 3; i++)
        {
            images[i].enabled = false;
        }
    }
}
