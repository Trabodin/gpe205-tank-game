﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : UIItem {

    public Image bar;
    float MaxHP;
    // Use this for initialization
    void Start () {

        MaxHP = player.engine.data.maxHealth;

    }
	
    public override void UpdateUI()
    {
        float curHp = player.engine.data.Health;

        bar.fillAmount = curHp / MaxHP;
    }
}
