﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public List<Camera> cams = new List<Camera>();

    public static CameraManager CM;

    
	// Use this for initialization
	void Start () {
		if(CM == null)
        {
            CM = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(CM != null)
        {
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

 

}
