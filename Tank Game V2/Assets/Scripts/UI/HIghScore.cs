﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HIghScore : UIItem {
    public Text text;
    string HighScoreNum = "";
    // Use this for initialization
    void Start () {
        HighScoreNum = PlayerPrefs.GetInt("Score").ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void UpdateUI()
    {
        if(player.engine.data.Score > PlayerPrefs.GetInt("Score"))
        {
            PlayerPrefs.SetInt("Score", player.engine.data.Score);
        }
        HighScoreNum = PlayerPrefs.GetInt("Score").ToString();
      
        text.text = "HighScore : " + HighScoreNum;
    }

}

  

