﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public abstract class UIItem : MonoBehaviour {

    public abstract void UpdateUI();

   
    public PlayerConroller player;
    private void Start()
    {

        player.UI.Add(this);

    }

}
