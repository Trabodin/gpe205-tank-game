﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour{

    public float damage;
    public float speed;
    public float dieTime;
    public Sound hitsound;
   // public AudioClip HitSound;
    public TankEngine Shooter;

    private Transform tf;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
        Destroy(gameObject, dieTime);
        StartCoroutine("MoveBullet");
	}
	
    IEnumerator MoveBullet()
    {
        //moves the bullet forward untill it is destroyed basically
        while (true)
        {
            tf.Translate(Vector3.forward * speed * Time.deltaTime);
            yield return null;
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        //if the other object has a pawn component/ if it is an enemie then call the pawns on hit function
        Controller obj = other.gameObject.GetComponent<Controller>();
        if (obj != null)
        {
            obj.Onhit(damage,Shooter);

        }
        SoundManager.soundManager.PlaySoundAtPoint(hitsound, tf.position);
       // AudioSource.PlayClipAtPoint(HitSound, tf.position);
        Destroy(gameObject);
    }

}
