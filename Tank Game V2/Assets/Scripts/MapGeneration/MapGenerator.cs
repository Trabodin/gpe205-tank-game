﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    public static MapGenerator generator;

    public bool UseMapOfTheDay =true;
    public int MapSeed;
    public float TileSpacing;
    [Range(2,20)]
    public int MapSizeX;
    [Range(2, 20)]
    public int MapSizeY;

    public Tile[,] tileGrid;

    public List<GameObject> Tiles;

    public PlayerConroller.ControlScheme[] playerControls = new PlayerConroller.ControlScheme[2] { PlayerConroller.ControlScheme.WASD,PlayerConroller.ControlScheme.ArrowKeys };

    //Object to chhild all the tiles to
    public GameObject WorldHolder;
    public GameObject PlayersHolder;
	// Use this for initialization
	void Start () {
        Gamemanager.instance.generator = this;
        //GenerateMap();
        if(generator == null)
        {
            generator = this;
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
    public void GenerateMap()
    {
        //clear out the map before adding anything
        //GameObject[] clearList = WorldHolder.GetComponentsInChildren<>();
        //foreach  (GameObject obj in clearList)
        //{
        //    Destroy(obj);
        //}

        //make a grid to hold the tile objects to use later
        tileGrid = new Tile[MapSizeX, MapSizeY];
        //seed the rand function 
        if (UseMapOfTheDay)
        {
            UnityEngine.Random.InitState(MapOfTheDaySeed());
        }
        else
        {
            UnityEngine.Random.InitState(MapSeed);
        }

        //for every spot in the grid make a tile
        for (int x = 0; x < MapSizeX; x++)
        {
            for (int y = 0; y < MapSizeY; y++)
            {
                float xPosition = x * TileSpacing;
                float yPosition = y * TileSpacing;
                Vector3 position = new Vector3(xPosition, 0, yPosition);

                CreateTile(position, x, y);
            }
        }

        SpawnAllPlayer();

    }

    public void SpawnAllPlayer()
    {
        List<Camera> cams = new List<Camera>();
        for (int i = 0; i < Gamemanager.instance.NumberOfPlayers; i++)
        {
            //spawn the player at one of those tiles
            int PlayerTileX = UnityEngine.Random.Range(0, MapSizeX);
            int PlayerTileY = UnityEngine.Random.Range(0, MapSizeY);

            GameObject temp = tileGrid[PlayerTileX, PlayerTileY].SpawnPlayer();
            temp.transform.SetParent(PlayersHolder.transform);
            PlayerPawn pawn = temp.GetComponent<PlayerPawn>();
            pawn.InstatiateCameras();
            pawn.ResizeCameras(i);
            PlayerConroller player = temp.GetComponent<PlayerConroller>();
            player.controlScheme = playerControls[i];
        }
    }

    public void SpawnPlayer(PlayerConroller player)
    {
        int PlayerTileX = UnityEngine.Random.Range(0, MapSizeX);
        int PlayerTileY = UnityEngine.Random.Range(0, MapSizeY);

        player.engine.tf.position = tileGrid[PlayerTileX, PlayerTileY].playerSpawner.transform.position;
    }
    //Gets the number of days since 2000 so theresa new seed vereryday
    private int MapOfTheDaySeed()
    {
        DateTime startDate = new DateTime(2000, 1, 1);
        int numberOfDays = (int)(DateTime.Today - startDate).TotalDays;
        return numberOfDays;
    }

    void CreateTile(Vector3 position,int x,int y)
    {
        GameObject chosenroom = Tiles[UnityEngine.Random.Range(0, Tiles.Count)];

        Tile temp = Instantiate(chosenroom, position, Quaternion.identity).GetComponent<Tile>();
        OpenDoors(temp ,x, y);
        tileGrid[x, y] = temp;
        temp.transform.SetParent(WorldHolder.transform);
    }

    //spawns the enemies for each time
    void EnemiesSpawn()
    {
        for (int x = 0; x < MapSizeX; x++)
        {
            for (int y = 0; y < MapSizeY; y++)
            {
                tileGrid[x, y].SpawnEnemies();
            }
        }
     
    }
    void OpenDoors(Tile tile ,int x, int y)
    {
        //0 = north and increase clockwise 1 east 2 south 3 west
        //if at top only open bottom door and only open top door when at the bottom
        if(x == 0)
        {
            tile.doors[0].SetActive(false);
        }
        else if(x == MapSizeX - 1)
        {
            tile.doors[2].SetActive(false);
        }
        else
        {
            tile.doors[0].SetActive(false);
            tile.doors[2].SetActive(false);
        }
        //repeat for left and right
        if(y == 0)
        {
            tile.doors[3].SetActive(false);

        }else if(y == MapSizeY - 1)
        {
            tile.doors[1].SetActive(false);
        }
        else
        {
            tile.doors[3].SetActive(false);
            tile.doors[1].SetActive(false);
        }
    }

    public void ToggleMapOfTheDay() { UseMapOfTheDay = !UseMapOfTheDay; }

    public void clearMap()
    {
        foreach (Transform item in WorldHolder.GetComponentsInChildren<Transform>())
        {
            if(item.gameObject != WorldHolder)
                Destroy(item.gameObject);
        }
        foreach (Transform item in PlayersHolder.GetComponentsInChildren<Transform>())
        {
            if (item.gameObject != PlayersHolder)
                Destroy(item.gameObject);
        }
    }
}
