﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//holds the doors and spawners
public class Tile : MonoBehaviour {

    public List<GameObject> doors;
    public List<SpawnerAITank> enemySpawners;
    public Spawner playerSpawner;

    private void Start()
    {
        SpawnEnemies();
    }

    public void SpawnEnemies()
    {
        foreach (SpawnerAITank spawner in enemySpawners)
        {
            spawner.spawn();
        }
    }

    public GameObject SpawnPlayer()
    {
        return playerSpawner.spawn();
    }
}
