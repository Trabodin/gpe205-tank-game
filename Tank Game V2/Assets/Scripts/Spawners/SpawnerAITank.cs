﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerAITank : Spawner {
    public GameObject PatrolPointParent;
    private List<Transform> PatrolPath;

	// Use this for initialization
	public override void Start () {


        base.Start();
        
    }

    public override GameObject spawn()
    {
        //gets the patrol path to give to the newly spawned tank
        PatrolPath = new List<Transform>();
        Transform[] tolist = PatrolPointParent.GetComponentsInChildren<Transform>();
        for (int i = 0; i < tolist.Length; i++)
        {
            PatrolPath.Add(tolist[i]);
        }
       

        GameObject TanktoBeSpawned = Instantiate(ItemToSpawn, transform.position, Quaternion.identity);
        AIController temp = TanktoBeSpawned.GetComponent<AIController>();
        temp.patrolPoints = PatrolPath;
        TanktoBeSpawned.transform.SetParent(transform.parent);
        return TanktoBeSpawned;
    }
}
