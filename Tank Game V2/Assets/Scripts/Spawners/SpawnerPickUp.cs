﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPickUp : Spawner
{

    public bool PickUpHasBeenTaken = true;

    public float RespawnSpeed;
    new private void Start()
    {
        base.Start();
        SpawnPickUp();
        PickUpHasBeenTaken = false;
    }
    private void Update()
    {
        if (PickUpHasBeenTaken)
        {
            PickUpHasBeenTaken = false;
            StartWaitTime();
        }
    }
    void StartWaitTime()
    {
        StartCoroutine("WaitTime");
    }

    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(RespawnSpeed);
        SpawnPickUp();

    }

    private void SpawnPickUp()
    {
        PickUp temp = Instantiate(ItemToSpawn, tf.position, Quaternion.identity).GetComponent<PickUp>();
        temp.spawner = this;
        temp.transform.SetParent(transform.parent);
    }
}
