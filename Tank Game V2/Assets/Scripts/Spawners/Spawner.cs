﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    protected Transform tf;
    [SerializeField]
    protected GameObject ItemToSpawn;

    // Use this for initialization
    public virtual void Start() {
        tf = GetComponent<Transform>();
    }

    public virtual GameObject spawn(){
        return Instantiate(ItemToSpawn, transform.position, Quaternion.identity);
    }
}
